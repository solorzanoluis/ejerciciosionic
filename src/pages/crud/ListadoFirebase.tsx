import { IonButton, IonButtons, IonCard, IonCardContent, IonCardHeader, IonCardSubtitle, IonCardTitle, IonContent, IonHeader, 
    IonImg, IonInput, IonItem, IonLabel, IonList, IonPage, IonRouterLink, IonRow, IonTitle, IonToolbar, useIonViewWillEnter } from "@ionic/react";
import { collection, getDocs, getFirestore , deleteDoc, doc, addDoc, updateDoc} from "firebase/firestore";
import React, { useState } from "react";
import { RouteComponentProps, useHistory } from "react-router-dom";
import { producto } from "../modelo/productos";
import swal from 'sweetalert';
import './estilo.css';
import { initializeApp } from "firebase/app";
import { getAuth, signOut } from "firebase/auth";

const ListarFirebase : React.FC = () =>{

    const firebaseApp = initializeApp({
        apiKey: "AIzaSyD_D84yaRWhkyalQgErNifrSPhWrbT3U9s",
        authDomain: "crudionic-a43d5.firebaseapp.com",
        databaseURL: "https://crudionic-a43d5-default-rtdb.firebaseio.com",
        projectId: "crudionic-a43d5",
        storageBucket: "crudionic-a43d5.appspot.com",
        messagingSenderId: "289561271738",
        appId: "1:289561271738:web:0ba00eb1b69547dcef9183",
        measurementId: "G-S5VF0K8VM4"
    });

    const db = getFirestore();

    
    const history = useHistory();

    const [listaProd, setlistaProducto] = useState<producto[]>([]);
    
    const mostrar = async() => {

        let lista: producto[] = [];
        const consulta = await getDocs(collection(db, "productos"));
        consulta.forEach((doc) => {
            /*console.log(`${doc.id} => ${doc.data().Nombre},${doc.data().Descripcion},${doc.data().Precio}`);*/
            let datos = {id: doc.id, nombre:doc.data().Nombre, descripcion: doc.data().Descripcion, precio:doc.data().Precio}
            //console.log(datos);
            lista.push(datos);
        });
        setlistaProducto(lista);
    }

    useIonViewWillEnter(()=>{
        mostrar();
    })

    const eliminar = async(id:string) =>{
        
        swal({
            title: "¿Estás seguro de eliminar este registro?",
            text: "¡Una vez eliminado, no podrá recuperar este archivo!",
            icon: "warning",
            //buttons: true,
            dangerMode: true,
          }).then((eliminar) => {
            if (eliminar) {
              swal("Registro eliminado exitosamente.", {
                icon: "success",
                });
                deleteDoc(doc(db, "productos", id));
                mostrar();
            } else {
              //swal("Operación cancelada");
              mostrar();

            }
          });
    }

    function actualizar(id: string, nom:string){
        history.replace('/actualizar/' + id +'/' + nom);
    }

    function cerrarsesion(){

        const auth = getAuth();

        signOut(auth).then(() => {
            history.replace('/SignIn');
        }).catch((error) => {
        
        });
    }

    return <IonPage>
       <IonHeader>
            <IonToolbar color="secondary">
                <IonTitle> Listado de productos</IonTitle>
                <IonButtons slot="end">
                    <IonButton href="/firebase">+</IonButton>
                </IonButtons>
            </IonToolbar>
        </IonHeader>

        <IonContent>

           
            {/* <IonButton class="btn" onClick={cerrarsesion}>Cerrar Sesión</IonButton> */}
            <IonButtons>
                <IonButton class="btn" onClick={cerrarsesion}>Cerrar Sesión</IonButton>
            </IonButtons>
            

            <IonList>

                {listaProd.map(producto =>(

                    

                    <IonCard key={producto.id}>

                        <IonImg class="imagen" src="assets/img/2.jpg"></IonImg>

                        <IonCardTitle class="texto">Nombre del producto: {producto.nombre}</IonCardTitle><p></p>
                        
                        <IonCardSubtitle class="texto">Precio: {producto.precio}</IonCardSubtitle>
                        <h3 id="texto">Descripción del producto.</h3>
                        <IonCardContent>
                         {producto.descripcion}
                        </IonCardContent>
                        

                        <IonRow class="botones">
                            <IonButton color="success" onClick={() => actualizar(producto.id as string,producto.nombre as string)}>
                                <h1 className="text">Actualizar</h1>
                            </IonButton><p/>
                            <IonButton color="danger" onClick={() => eliminar(producto.id as string)}>
                            <h1 className="text">Eliminar</h1>
                            </IonButton>
                        </IonRow>

                    </IonCard>
                ))}

            </IonList>
        </IonContent>
     
    </IonPage>
}

export default ListarFirebase;