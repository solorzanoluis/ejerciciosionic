import React, { useRef, useState } from "react";
import { IonButton, IonButtons, IonCard, IonContent, IonHeader, IonInput, IonItem, IonLabel, IonPage, IonRow, IonTitle, IonToolbar, useIonViewWillEnter } from "@ionic/react";


import { initializeApp } from "firebase/app"
import { doc, DocumentData, getDoc, getDocs, getFirestore, query, QuerySnapshot, updateDoc, where } from "firebase/firestore"
import { collection, addDoc } from "firebase/firestore";
import { NavLink, RouteComponentProps, useHistory, useParams } from "react-router-dom";
import { producto } from "../modelo/productos";
// import './estilo.css';
import swal from 'sweetalert';




const ActualizarFirebase: React.FC = () =>{

    const firebaseApp = initializeApp({
        apiKey: "AIzaSyD_D84yaRWhkyalQgErNifrSPhWrbT3U9s",
        authDomain: "crudionic-a43d5.firebaseapp.com",
        databaseURL: "https://crudionic-a43d5-default-rtdb.firebaseio.com",
        projectId: "crudionic-a43d5",
        storageBucket: "crudionic-a43d5.appspot.com",
        messagingSenderId: "289561271738",
        appId: "1:289561271738:web:0ba00eb1b69547dcef9183",
        measurementId: "G-S5VF0K8VM4"
    });
    
    const db = getFirestore();

    const history = useHistory();

    // let id_prod= "Qqcb7RX8fT8dG7oQlsxl";
    // let nom_prod = 'Computadora';

    let id_prod = '';
    let nom_prod = '';

    // console.log(id_prod);
    // console.log(nom_prod);

    const id = useParams();//Inicializamos useParams y recibimos los parametros enviados en la url
    //let val = Object.entries(id);

    // let va1 = val[0];
    // let val2 = val[1];
    // console.log('valor id ' + val[0]);
    // console.log('valor nombre ' + val[1]);

    for (const [key, value] of Object.entries(id)) {//convertimos en matriz[key,value] a ese objeto y obtenemos los valores valor
        //console.log(key);

        if (key === 'id'){
            id_prod = value as string;
        } else {
            nom_prod = value as string;
        }
        //nom_prod = value as string;
    }

    // console.log('id = ' + id_prod);
    // console.log('nom = ' + nom_prod);


    //---------------------------Mostrar el producto---------------------------------

    const [listaProd, setlistaProducto] = useState<producto[]>([]);
    
    const mostrar = async() => {

        let lista: producto[] = [];

        const consulta = query(collection(db, "productos"), where("Nombre", "==", nom_prod));

        const ejecutar = await getDocs(consulta);

        ejecutar.forEach((doc) => {

            
            let datos = {id: doc.id, nombre:doc.data().Nombre, descripcion: doc.data().Descripcion, precio:doc.data().Precio}
            // //console.log(datos);
            lista.push(datos);
        });

        setlistaProducto(lista);
        //console.log(lista);
    }

    useIonViewWillEnter(()=>{ //Ejecutamos el metodo en automático
        mostrar();
    })
   

    //-----------------------Actualizar-----------------------
    const nombreRef = useRef<HTMLIonInputElement>(null);
    const descripcionRef = useRef<HTMLIonInputElement>(null);
    const precioRef = useRef<HTMLIonInputElement>(null);

    const produc = doc(db, "productos", id_prod);//Obtenemos los datos de ese id

    console.log("Este es el product "+ Object.entries(produc));

    const actualizar = async () =>{

        const nom = nombreRef.current!.value;
        const desc = descripcionRef.current!.value;
        const prec = precioRef.current!.value;

        
        await updateDoc(produc, {
            Nombre: nom,
            Descripcion: desc,
            Precio: prec
        });

        swal({
            title: "Registro actualizado exitosamente",
            icon: "success",
        });
        
        history.replace('/mostrar');
    }

    

    return <IonPage>

        <IonHeader>
            <IonToolbar color="secondary">
                <IonTitle>Actualizar producto</IonTitle>
            </IonToolbar>
        </IonHeader>

        <IonContent>

                {listaProd.map(producto =>(

               

                    <form className="ion-padding" key={producto.id}>
                    <IonItem>
                        <IonLabel position="floating">Nombre del producto</IonLabel>
                        <IonInput value={producto.nombre} ref={nombreRef} />
                    </IonItem>

                    <IonItem>
                        <IonLabel position="floating">Precio</IonLabel>
                        <IonInput value={producto.precio} ref={precioRef} />
                    </IonItem>

                    <IonItem>
                        <IonLabel position="floating">Descripción</IonLabel>
                        <IonInput value={producto.descripcion} ref={descripcionRef} />
                    </IonItem>

                    <IonRow class="botones">
                        <IonButton onClick={actualizar}>
                            <h1 className="text">Actualizar</h1>
                        </IonButton><p/>
                        <IonButton color="success" onClick={() => history.replace('/mostrar')}>
                            <h1 className="text">Cancelar</h1>
                        </IonButton>
                    </IonRow>
                    </form>

                  

                ))}
           

        </IonContent>

    </IonPage>
    
}

export default ActualizarFirebase;