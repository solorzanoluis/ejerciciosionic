import React, { useRef, useState } from "react";
import { IonButton, IonButtons, IonContent, IonHeader, IonInput, IonItem, IonLabel, IonPage, IonRow, IonTitle, IonToolbar } from "@ionic/react";


import { initializeApp } from "firebase/app"
import { Firestore, getFirestore } from "firebase/firestore"
import { collection, addDoc } from "firebase/firestore";
import { RouteComponentProps, useHistory } from "react-router-dom";
import './estilo.css';

//inicializamos firebase
// if(!firebase.getApps.length){
//     firebase.initializeApp(firebaseConfig);
// }



const firebaseApp = initializeApp({
    apiKey: "AIzaSyD_D84yaRWhkyalQgErNifrSPhWrbT3U9s",
    authDomain: "crudionic-a43d5.firebaseapp.com",
    databaseURL: "https://crudionic-a43d5-default-rtdb.firebaseio.com",
    projectId: "crudionic-a43d5",
    storageBucket: "crudionic-a43d5.appspot.com",
    messagingSenderId: "289561271738",
    appId: "1:289561271738:web:0ba00eb1b69547dcef9183",
    measurementId: "G-S5VF0K8VM4"
});

const db = getFirestore();


const AgregarFirebase: React.FC = () =>{

    const history = useHistory();

   
    const nombreRef = useRef<HTMLIonInputElement>(null);
    const descripcionRef = useRef<HTMLIonInputElement>(null);
    const precioRef = useRef<HTMLIonInputElement>(null);

    const guardar = async () =>{

        const nom = nombreRef.current!.value;
        const desc = descripcionRef.current!.value;
        const prec = precioRef.current!.value;

        try {

            await addDoc(collection(db, "productos"), {
                //Id = Firestore.FieldValue
                Nombre: nom,
                Descripcion: desc,
                Precio: prec
            });
            //console.log("Document written with ID: ", docRef.id);
            history.push('/mostrar');
        } catch (e) {
            console.error("Error adding document: ", e);
        }
    }

    

    return <IonPage>

        <IonHeader>
            <IonToolbar color="secondary">
                <IonTitle>Registrar Nuevo producto</IonTitle>
            </IonToolbar>
        </IonHeader>

        <IonContent>

            <form className="ion-padding">
                <IonItem>
                    <IonLabel position="floating">Nombre del producto</IonLabel>
                    <IonInput ref={nombreRef} />
                </IonItem>
                
                <IonItem>
                    <IonLabel position="floating">Precio</IonLabel>
                    <IonInput ref={precioRef} />
                </IonItem>

                <IonItem>
                    <IonLabel position="floating">Descripción</IonLabel>
                    <IonInput ref={descripcionRef} />
                </IonItem>

                <IonRow class="botones">
                    <IonButton onClick={guardar}>
                        <h1 className="text">Agregar</h1>
                    </IonButton><p/>
                    <IonButton color="success" href="/mostrar">
                        <h1 className="text">Cancelar</h1>
                    </IonButton>
                </IonRow>
            </form>

        </IonContent>

    </IonPage>
    
}

export default AgregarFirebase;