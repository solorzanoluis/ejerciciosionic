import React from 'react';
import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar, IonSegment, IonSegmentButton, IonLabel, IonIcon, IonButton, IonItemDivider, IonList, IonItem } from '@ionic/react';
import { call, camera, bookmark, heart, pin } from 'ionicons/icons';

export const Pruebas: React.FC = () => {
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>SegmentButton</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        {/*-- Segment buttons with text and click listener --*/}
        <IonSegment onIonChange={(e) => console.log(`${e.detail.value} segment selected`)}>
          <IonSegmentButton value="Friends">
            <IonLabel>Friends</IonLabel>
          </IonSegmentButton>
          <IonSegmentButton value="Enemies">
            <IonLabel>Enemies</IonLabel>
          </IonSegmentButton>
        </IonSegment>

        {/*-- Segment buttons with the first checked and the last disabled --*/}
        <IonSegment value="paid">
          <IonSegmentButton value="paid">
            <IonLabel>Paid</IonLabel>
          </IonSegmentButton>
          <IonSegmentButton value="free">
            <IonLabel>Free</IonLabel>
          </IonSegmentButton>
          <IonSegmentButton disabled value="top">
            <IonLabel>Top</IonLabel>
          </IonSegmentButton>
        </IonSegment>

        {/*-- Segment buttons with values and icons --*/}
        <IonSegment>
          <IonSegmentButton value="camera">
            <IonIcon icon={camera} />
          </IonSegmentButton>
          <IonSegmentButton value="bookmark">
            <IonIcon icon={bookmark} />
          </IonSegmentButton>
        </IonSegment>

        {/*-- Segment with a value that checks the last button --*/}
        <IonSegment value="shared">
          <IonSegmentButton value="bookmarks">
            <IonLabel>Bookmarks</IonLabel>
          </IonSegmentButton>
          <IonSegmentButton value="reading">
            <IonLabel>Reading List</IonLabel>
          </IonSegmentButton>
          <IonSegmentButton value="shared">
            <IonLabel>Shared Links</IonLabel>
          </IonSegmentButton>
        </IonSegment>

        {/*-- Label only --*/}
        <IonSegment value="1">
          <IonSegmentButton value="1">
            <IonLabel>Item One</IonLabel>
          </IonSegmentButton>
          <IonSegmentButton value="2">
            <IonLabel>Item Two</IonLabel>
          </IonSegmentButton>
          <IonSegmentButton value="3">
            <IonLabel>Item Three</IonLabel>
          </IonSegmentButton>
        </IonSegment>

        {/*-- Icon only --*/}
        <IonSegment value="heart">
          <IonSegmentButton value="call">
            <IonIcon icon={call} />
          </IonSegmentButton>
          <IonSegmentButton value="heart">
            <IonIcon icon={heart} />
          </IonSegmentButton>
          <IonSegmentButton value="pin">
            <IonIcon icon={pin} />
          </IonSegmentButton>
        </IonSegment>

        {/*-- Icon top --*/}
        <IonSegment value="2">
          <IonSegmentButton value="1">
            <IonLabel>Item One</IonLabel>
            <IonIcon icon={call} />
          </IonSegmentButton>
          <IonSegmentButton value="2">
            <IonLabel>Item Two</IonLabel>
            <IonIcon icon={heart} />
          </IonSegmentButton>
          <IonSegmentButton value="3">
            <IonLabel>Item Three</IonLabel>
            <IonIcon icon={pin} />
          </IonSegmentButton>
        </IonSegment>

        {/*-- Icon bottom --*/}
        <IonSegment value="1">
          <IonSegmentButton value="1" layout="icon-bottom">
            <IonIcon icon={call} />
            <IonLabel>Item One</IonLabel>
          </IonSegmentButton>
          <IonSegmentButton value="2" layout="icon-bottom">
            <IonIcon icon={heart} />
            <IonLabel>Item Two</IonLabel>
          </IonSegmentButton>
          <IonSegmentButton value="3" layout="icon-bottom">
            <IonIcon icon={pin} />
            <IonLabel>Item Three</IonLabel>
          </IonSegmentButton>
        </IonSegment>

        {/*-- Icon start --*/}
        <IonSegment value="1">
          <IonSegmentButton value="1" layout="icon-start">
            <IonLabel>Item One</IonLabel>
            <IonIcon icon={call} />
          </IonSegmentButton>
          <IonSegmentButton value="2" layout="icon-start">
            <IonLabel>Item Two</IonLabel>
            <IonIcon icon={heart} />
          </IonSegmentButton>
          <IonSegmentButton value="3" layout="icon-start">
            <IonLabel>Item Three</IonLabel>
            <IonIcon icon={pin} />
          </IonSegmentButton>
        </IonSegment>

        {/*-- Icon end --*/}
        <IonSegment value="1">
          <IonSegmentButton value="1" layout="icon-end">
            <IonIcon icon={call} />
            <IonLabel>Item One</IonLabel>
          </IonSegmentButton>
          <IonSegmentButton value="2" disabled layout="icon-end">
            <IonIcon icon={heart} />
            <IonLabel>Item Two</IonLabel>
          </IonSegmentButton>
          <IonSegmentButton value="3" layout="icon-end">
            <IonIcon icon={pin} />
            <IonLabel>Item Three</IonLabel>
          </IonSegmentButton>
        </IonSegment>
      


      <h3>Default</h3>
      <p>
        <IonButton>Default</IonButton>
        <IonButton><IonIcon slot="icon-only" name="star"></IonIcon></IonButton>
        <IonButton><IonIcon slot="start" name="pin"></IonIcon>Map</IonButton>
        <IonButton shape="round">Round</IonButton>
        <IonButton fill="outline">Outline</IonButton>
        <IonButton fill="clear">Clear</IonButton>
        <IonButton fill="outline" expand="full">Full Outline Full Outline Full Outline Full Outline Full Outline Full Outline Full Outline</IonButton>
        <IonButton fill="clear" expand="block">Block Clear</IonButton>
      </p>

      <h3>Round button combinations</h3>
      <p>
        <IonButton shape="round" size="small">Round & Small</IonButton>
        <IonButton shape="round" size="large">Round & Large</IonButton>
        <IonButton shape="round" fill="outline">Round & Outline</IonButton>
      </p>

      <IonButton  class="wide">wide</IonButton>
      <IonButton class="large">large</IonButton>
      <IonButton  class="round">rounded</IonButton>
      <IonButton  aria-label="this is my custom label">custom aria-label</IonButton>

      {/* <!-- Custom Colors --> */}
      <IonButton  class="custom">custom</IonButton>
      <IonButton  class="custom ion-activated ion-focused">custom.focused</IonButton>
      <IonButton  class="custom ion-activated">custom.activated</IonButton>
      <IonButton  color="secondary" class="custom">custom w/ secondary</IonButton>
      <IonButton  fill="clear" class="medium">custom medium</IonButton>



      <h1>Divisoress</h1>



        <IonItemDivider>
          <IonLabel>
            Basic Item Divider
          </IonLabel>
        </IonItemDivider>

      <IonItemDivider color="secondary">
        <IonLabel>
          Secondary Item Divider
        </IonLabel>
      </IonItemDivider>

      {/*-- Item Dividers in a List --*/}
      <IonList>
        <IonItemDivider>
          <IonLabel>
            Section A
          </IonLabel>
        </IonItemDivider>

        <IonItem><IonLabel>A1</IonLabel></IonItem>
        <IonItem><IonLabel>A2</IonLabel></IonItem>
        <IonItem><IonLabel>A3</IonLabel></IonItem>
        <IonItem><IonLabel>A4</IonLabel></IonItem>
        <IonItem><IonLabel>A5</IonLabel></IonItem>

        <IonItemDivider>
          <IonLabel>
            Section B
          </IonLabel>
        </IonItemDivider>

        <IonItem><IonLabel>B1</IonLabel></IonItem>
        <IonItem><IonLabel>B2</IonLabel></IonItem>
        <IonItem><IonLabel>B3</IonLabel></IonItem>
        <IonItem><IonLabel>B4</IonLabel></IonItem>
        <IonItem><IonLabel>B5</IonLabel></IonItem>
      </IonList>

    </IonContent>
    </IonPage>
  );
};