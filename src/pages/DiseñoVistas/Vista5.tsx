import { IonButton, IonButtons, IonCard, IonCol, IonContent, IonFab, IonFabButton, IonGrid, IonHeader, IonIcon, IonImg, IonPage, IonRow, IonTitle, IonToolbar } from "@ionic/react";
import { add, arrowBack } from "ionicons/icons";
import React from "react";
import { useHistory } from "react-router";
import './estilos/Vista5.css';

const Vista5: React.FC = () =>{

    const history = useHistory();

    return <IonPage>

        <IonHeader class="no-border" >
            <IonToolbar class="colorToolV4"  color="medium" >
                <IonButtons slot="start" onClick={() => history.push('/Vista4')}>
                    <IonIcon icon={arrowBack}/>
                </IonButtons>
                <IonTitle class="ion-text-center">Compare Cars</IonTitle>
                <IonButtons slot="end" class="">
                    Edit
                </IonButtons>
            </IonToolbar>
        </IonHeader>

        <IonContent>

            <IonGrid class="gridV4">
                <IonRow>
                    <IonCol class="col1V5">
                        <IonCard class="cardV5">
                            <IonImg class="imgV4" src="assets/img/autos/Porsche2 718.png"></IonImg>
                        </IonCard>
                    </IonCol>

                    <IonCol class="col2V4">
                        <div className="col2V4">
                            <h6 className="txt1col2V4">Porsche 718</h6>
                            <h6 className="txt2col2V4">Porsche/Luxury/The 2.3L EcoBoost</h6>
                            <h6 className="txt3col2V4">$62,000.00 - 74,000.00</h6>
                        </div>
                    </IonCol>
                </IonRow>

                <div className="btn2V5">
                    <IonRow>
                        <IonCol>
                            <hr className="hrV5"></hr>
                            {/* <IonButton class="btn02V5">
                                <div className="txt2btnV5">
                                    vs
                                </div>
                            </IonButton> */}
                            <IonFab horizontal="center" vertical="center">
                                <IonFabButton  class="btn02V5">
                                    <div className="txt2btnV5">
                                        vs
                                    </div>
                                </IonFabButton>
                            </IonFab>
                            
                        </IonCol>
                    </IonRow>
                </div>
            </IonGrid>

            <IonGrid class="gridV5">
                <IonRow>
                    <IonCol class="col1V5">
                        <IonCard class="cardV5">
                            <IonImg class="imgV4" src="assets/img/autos/continental.png"></IonImg>
                        </IonCard>
                    </IonCol>

                    <IonCol class="col2V4">
                        <div className="col2V4">
                            <h6 className="txt1col2V4">Continental</h6>
                            <h6 className="txt2col2V4">Bentley/Luxury/The 2.3L EcoBoost</h6>
                            <h6 className="txt3col2V4">$42,70 - $48,70</h6>
                        </div>
                    </IonCol>
                </IonRow>

                <div className="btn2V5">
                    <IonRow>
                        <IonCol>
                            <hr className="hrV5"></hr>
                            <IonFab horizontal="center" vertical="center">
                                <IonFabButton  class="btn02V5">
                                    <div className="txt2btnV5">
                                        vs
                                    </div>
                                </IonFabButton>
                            </IonFab>
                        </IonCol>
                    </IonRow>
                </div>

            </IonGrid>

            <IonGrid class="gridV5">
                <IonRow>
                    <IonCol class="col1V5">
                        <IonCard class="cardV5">
                            <IonImg class="imgV4" src="assets/img/autos/LexusLC.png"></IonImg>
                        </IonCard>
                    </IonCol>

                    <IonCol class="col2V4">
                        <div className="col2V4">
                            <h6 className="txt1col2V4">Lexus LC</h6>
                            <h6 className="txt2col2V4">Lexus/Luxury/The 2.3L EcoBoost</h6>
                            <h6 className="txt3col2V4">$56,000.00 - 66,000.00</h6>
                        </div>
                    </IonCol>
                </IonRow>

                <div className="btn2V5">
                    <IonRow>
                        <IonCol>
                            <hr className="hrV5"></hr>
                            <IonFab horizontal="center" vertical="center">
                                <IonFabButton  class="btn02V5">
                                    <div className="txt2btnV5">
                                        vs
                                    </div>
                                </IonFabButton>
                            </IonFab>
                        </IonCol>
                    </IonRow>
                </div>

            </IonGrid>

            <IonGrid class="gridV5">
                <IonRow>
                    <IonCol class="col1V5">
                        <IonCard class="cardV5">
                            <IonImg class="imgV4" src="assets/img/autos/polestar.png"></IonImg>
                        </IonCard>
                    </IonCol>

                    <IonCol class="col2V4">
                        <div className="col2V4">
                            <h6 className="txt1col2V4">Polestar 1</h6>
                            <h6 className="txt2col2V4">Lexus/Luxury/The 2.3L EcoBoost</h6>
                            <h6 className="txt3col2V4">$44,000.00 - 48,000.00</h6>
                        </div>
                    </IonCol>
                </IonRow>

                <div className="btn2V5">
                    <IonRow>
                        <IonCol>
                            <hr className="hrV5"></hr>
                            <IonFab horizontal="center" vertical="center">
                                <IonFabButton  class="btn02V5">
                                    <div className="txt2btnV5">
                                        vs
                                    </div>
                                </IonFabButton>
                            </IonFab>
                        </IonCol>
                    </IonRow>
                </div>

            </IonGrid>

            <div className="btnV5">
                <IonRow>
                    <IonCol>
                        <IonButton shape="round" size="default" fill="outline" color="success">
                            <div className="txtbtnV5">
                                <IonIcon class="iconV4" icon={add}></IonIcon>
                                Add Cars
                            </div>
                        </IonButton>
                    </IonCol>
                    <IonCol>
                        <IonButton shape="round" size="default" color="success" onClick={() => history.push('/Vista6')}>
                            <div className="txtbtnV5">
                                Compare
                            </div>
                        </IonButton>                   
                    </IonCol>
                </IonRow>
            </div>
            
        </IonContent>
        
    </IonPage>
}

export default Vista5;