import { IonButton, IonButtons, IonCard, IonCardSubtitle, IonCol, IonContent, IonFab, IonFabButton, IonGrid, IonHeader, IonIcon, IonImg, IonPage, IonRow, IonTitle, IonToolbar } from "@ionic/react";
import { add, arrowBack, star, starOutline } from "ionicons/icons";
import React from "react";
import { useHistory } from "react-router";
import './estilos/Vista6.css';

const Vista6 : React.FC = () =>{

    const history = useHistory();

    return <IonPage>

        <IonHeader class="no-border" >
            <IonToolbar class="colorToolV4"  color="medium" >
                <IonButtons slot="start" onClick={() => history.push('/Vista5')}>
                    <IonIcon icon={arrowBack}/>
                </IonButtons>
                <IonTitle class="ion-text-center">Cars Information</IonTitle>
                <IonButtons slot="end" class="">
                    Edit
                </IonButtons>
            </IonToolbar>
        </IonHeader>

        <IonContent>

            <IonCard class="card1V5">
                <IonImg src="assets/img/autos/porscheTaycan.png"></IonImg>
            </IonCard>

            <IonCard class="colorCards card2V5">
                <IonGrid>
                    <IonRow>
                        <IonCol class="col1V5">
                            <IonCard class="card3V5">
                                <IonImg class="imgV5" src="assets/img/autos/polestar.png"></IonImg>
                            </IonCard>
                        </IonCol>

                        <IonCol>
                            <h6 className="nomV6">Porsche Taycan</h6>
                            <h6 className="descV6">McLaren/Luxury</h6>
                        </IonCol>
                            
                        <IonCol class="col3V6">
                            <h6 className="precV6">$638,800</h6>
                            <h6 className="txtV6">Price</h6>
                        </IonCol>
                    </IonRow>
                </IonGrid>
            </IonCard>

            
            <h3 className="txtDesc">Porsche says that the name Taycan roughly translates to "lively young horse", paying homage to the leaping 
            horse that has been featured on the brand's crest logo since 1952.12</h3>


            <IonCard className="card4V6">
                <IonButtons class="">
                    <div className="txtCard4">Rate this new</div>
                    <IonIcon class="iconsV6" icon={starOutline}/>
                    <IonIcon class="iconsV6" icon={starOutline}/>
                    <IonIcon class="iconsV6" icon={starOutline}/>
                    <IonIcon class="iconsV6" icon={starOutline}/>
                    <IonIcon class="iconsV6" icon={starOutline}/>
                    
                </IonButtons>
            </IonCard>


            <h3 className="txt2V6">Related</h3>

            <IonCard class="colorCards card3V6">
                <IonGrid>
                    <IonRow>

                        <IonCol >
                            <h6 className="nom2V6">2019 Macan Facelift Launvhed; More Affordable Than Before</h6>
                            <h6 className="desc2V6">By Sonny Jul 29/2020</h6>
                        </IonCol>

                        <IonCol class="col2V6">
                            <IonCard class="card5V6">
                                <IonImg src="assets/img/autos/PorscheMacan.png"></IonImg>
                                <div className="favV5">
                                    <IonFab horizontal="end" vertical="bottom">
                                        <div className="txtbtnV6">
                                            12:24
                                        </div>
                                    </IonFab>
                                </div>
                                
                            </IonCard>
                        </IonCol>

                    </IonRow>
                </IonGrid>
            </IonCard>

            <IonCard class="colorCards card3V6">
                <IonGrid>
                    <IonRow>

                        <IonCol>
                            <h6 className="nom2V6">Porsche Shows Off New Electric Crossver Concept</h6>
                            <h6 className="desc2V6">By Schumacher Sept 18/2020</h6>
                        </IonCol>

                        <IonCol class="col2V6">
                            <IonCard class="card5V6">
                                <IonImg src="assets/img/autos/Porsche2.jpg"></IonImg>
                                <div className="favV5">
                                    <IonFab horizontal="end" vertical="bottom">
                                        <div className="txtbtnV6">
                                            08:28
                                        </div>
                                    </IonFab>
                                </div>
                                
                            </IonCard>
                        </IonCol>

                    </IonRow>
                </IonGrid>
            </IonCard>
               

        </IonContent>

    </IonPage>

}

export default Vista6;