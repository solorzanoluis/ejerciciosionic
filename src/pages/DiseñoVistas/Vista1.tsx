import { IonBackButton, IonButtons, IonCard, IonCardSubtitle, IonCardTitle, IonCol, IonContent, IonGrid, IonHeader, IonIcon, IonImg, IonInput, IonItem, IonLabel, IonList, IonPage, IonRow, IonSegment, IonSegmentButton, IonSlide, IonSlides, IonTitle, IonToggle, IonToolbar } from "@ionic/react";
import React from "react";
import "./estilos/estilos.css";
import { arrowBack, arrowDown, arrowForward, arrowRedo, arrowUndo, headset, heartCircleOutline, heartHalf, heartHalfSharp, heartOutline, openOutline, star } from "ionicons/icons";
import { useHistory } from "react-router";

const slideOpts = {
    // initialSlide: 0,
    // speed: 400
    slidesPerView : 1.5,
    //centerSlides: true,
    spaceBetween: 1,
    //utoplay: 0.2,
    //loop: true,
};



const Vista1: React.FC = ()=>{

    const history = useHistory();

    return <IonPage>

        <IonHeader >
            <IonToolbar>
                <IonButtons slot="start">
                    <IonIcon icon={arrowBack}/>
                </IonButtons>
                <IonTitle slot="start">Pantalla 1</IonTitle>

                <IonButtons slot="end" onClick={() => history.push('/Vista2')}>
                    <IonIcon icon={arrowForward}/>
                </IonButtons>
            </IonToolbar>
        </IonHeader>

        <IonContent color="medium" fullscreen>
            
            <div id="Colorcontent">
            
                <IonSlides class="ion-margin-top swiper-slide" options={slideOpts} color="primary">
                    <IonSlide>
                        <IonCard class="card" color="">
                            <IonIcon class="icono" slot="" icon={heartOutline} />
                            <IonImg id="img" className="img" src="assets/img/autos/Imagen2.png"></IonImg>
                            <IonCardTitle class="titulo">Porsche</IonCardTitle>
                            <IonCardSubtitle class="precios">$46,500</IonCardSubtitle>
                        </IonCard>
                    </IonSlide>
                    <IonSlide>
                        <IonCard class="card">
                            <IonIcon class="icono" slot="" icon={heartOutline} />
                            <IonImg id="img" src="assets/img/autos/Imagen3.png"></IonImg>
                            <IonCardTitle class="titulo">Porsche</IonCardTitle>
                            <IonCardSubtitle class="precios">$46,500</IonCardSubtitle>
                        </IonCard>
                    </IonSlide>

                    <IonSlide>
                        <IonCard class="card">
                            <IonIcon class="icono" slot="" icon={heartOutline} />
                            <IonImg id="img" src="assets/img/autos/Imagen4.png"></IonImg>
                            <IonCardTitle class="titulo">Porsche</IonCardTitle>
                            <IonCardSubtitle class="precios">$46,500</IonCardSubtitle>
                        </IonCard>
                    </IonSlide>

                    <IonSlide>
                        <IonCard class="card">
                            <IonIcon class="icono" slot="" icon={heartOutline} />
                            <IonImg id="img" src="assets/img/autos/Imagen5.png"></IonImg>
                            <IonCardTitle class="titulo">Porsche</IonCardTitle>
                            <IonCardSubtitle class="precios">$46,500</IonCardSubtitle>
                        </IonCard>
                    </IonSlide>

                    <IonSlide>
                        <IonCard class="card">
                            <IonIcon class="icono" slot="" icon={heartOutline} />
                            <IonImg id="img" src="assets/img/autos/Imagen6.png"></IonImg>
                            <IonCardTitle class="titulo">Porsche</IonCardTitle>
                            <IonCardSubtitle class="precios">$46,500</IonCardSubtitle>
                        </IonCard>
                    </IonSlide>
                    
                </IonSlides>


                {/* <IonSegment onIonChange={e => console.log('Segment selected', e.detail.value)} color="success" >
                    <IonSegmentButton class="t" value="all" id="txt_Segment">
                        <IonLabel>All</IonLabel>
                    </IonSegmentButton>
                    <IonSegmentButton class="t" value="sedan" id="txt_Segment">
                        <IonLabel>Sedan</IonLabel>
                    </IonSegmentButton>
                    <IonSegmentButton class="t" value="SUV" id="txt_Segment">
                        <IonLabel>Sub</IonLabel>
                    </IonSegmentButton>
                    <IonSegmentButton class="t" value="Luxury" id="txt_Segment">
                        <IonLabel>Luxury</IonLabel>
                    </IonSegmentButton>
                    
                </IonSegment> */}

                
                    <IonButtons class="menu">
                        <h6 onClick={()=>history.push('/Vista1')} className="menu">All</h6>
                        <h6 className="menuu">Sedan</h6>
                        <h6 className="menuu">SUV</h6>
                        <h6 className="menuu">Luxury</h6>
                    </IonButtons>
               


                <IonGrid>
                    <IonCard>
                        <IonRow>
                            <IonCol class="colImg">
                                <IonImg class="img2" src="assets/img/autos/Imagen5.png"></IonImg>
                            </IonCol> 
                            <IonCol class="colTxt">   
                                <h6 className="nombre">Porche 911</h6>
                                <h6 className="modelo">Porsche/Luxury</h6>
                                <h6 className="precios2">$634,000</h6>
                            </IonCol>
                        </IonRow>
                    </IonCard>

                    <IonCard>
                        <IonRow>
                            <IonCol class="colImg">
                                <IonImg class="img2" src="assets/img/autos/Imagen2.png"></IonImg>
                            </IonCol> 
                            <IonCol class="colTxt">   
                                <h6 className="nombre">Porche 911</h6>
                                <h6 className="modelo">Porsche/Luxury</h6>
                                <h6 className="precios2">$500,000</h6>
                            </IonCol>
                        </IonRow>
                    </IonCard>

                    <IonCard>
                        <IonRow>
                            <IonCol class="colImg">
                                <IonImg class="img2" src="assets/img/autos/Imagen3.png"></IonImg>
                            </IonCol> 
                            <IonCol class="colTxt">   
                                <h6 className="nombre">Porche 911</h6>
                                <h6 className="modelo">Porsche/Luxury</h6>
                                <h6 className="precios2">$500,000</h6>
                            </IonCol>
                        </IonRow>
                    </IonCard>

                    <IonCard>
                        <IonRow>
                            <IonCol class="colImg">
                                <IonImg class="img2" src="assets/img/autos/Imagen4.png"></IonImg>
                            </IonCol> 
                            <IonCol class="colTxt">   
                                <h6 className="nombre">Porche 911</h6>
                                <h6 className="modelo">Porsche/Luxury</h6>
                                <h6 className="precios2">$500,000</h6>
                            </IonCol>
                        </IonRow>
                    </IonCard>

                
                </IonGrid>


               
            </div>

            
        </IonContent>

    </IonPage>
}

export default Vista1;