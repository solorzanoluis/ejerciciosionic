import { IonButton, IonButtons, IonCard, IonCardContent, IonCardHeader, IonCardSubtitle, IonCardTitle, IonCol, IonContent, IonGrid, IonHeader, IonIcon, IonImg, IonPage, IonRow, IonTitle, IonToolbar } from "@ionic/react";
import { arrowBack, arrowForward, chevronDown, chevronForward, heartOutline, star, stopOutline } from "ionicons/icons";
import React from "react";
import { useHistory } from "react-router";
import './estilos/Vista3.css';

const Vista3: React.FC = () =>{

    const history = useHistory();

    return <IonPage>

        <IonHeader >
            <IonToolbar id="colorTool" color="medium toolV2">
                <IonButtons slot="start" onClick={() => history.push('/Vista2')}>
                    <IonIcon icon={arrowBack}/>
                </IonButtons>
                <IonTitle slot="start"></IonTitle>

                <IonButtons slot="end">
                    Bangkok
                    <IonIcon icon={chevronDown}/>
                    <div className="icons">
                        <IonIcon icon={heartOutline}/>
                    </div>
                    <div className="icons">
                        <IonIcon onClick={() => history.push('/Vista3')} icon={arrowForward}/>
                    </div>
                </IonButtons>
            </IonToolbar>
        </IonHeader>

        <IonContent>

            

            <div>

                <IonButtons class="menu">
                    <h6 onClick={()=>history.push('/Vista1')} className="menu">Price</h6>
                    <h6 className="menuu">Reviews</h6>
                    <h6 className="menuu">FAQ</h6>
                </IonButtons>

                <IonCard id="card">
                    <IonGrid>
                        <IonRow>
                            <IonCol id="col01">
                                <div id="col1">
                                    <h3 id="nomCol1">Cayman</h3>
                                    <h3 id="descCol1">1988 cc, Automatic petrol, 9.0 kmpl </h3>
                                </div>
                            </IonCol>
                        
                            <IonCol id="col02">
                            <div id="col2">
                                <h3 id="precCol2">$62,000.00</h3>
                                <div id="iconCol2">
                                    <IonButtons>
                                        Compare
                                        <IonIcon icon={stopOutline}/>
                                    </IonButtons>
                                </div>
                                </div>
                            </IonCol>
                        </IonRow> 
                    </IonGrid>
                </IonCard>

                <IonCard id="card">
                    <IonGrid>
                        <IonRow>
                            <IonCol id="col01">
                                <div id="col1">
                                    <h3 id="nomCol1">Boxter</h3>
                                    <h3 id="descCol1">1988 cc, Automatic petrol, 9.0 kmpl </h3>
                                </div>
                            </IonCol>
                        
                            <IonCol id="col02">
                            <div id="col2">
                                <h3 id="precCol2">$68,000.00</h3>
                                <div id="iconCol2">
                                    <IonButtons>
                                        Compare
                                        <IonIcon icon={stopOutline}/>
                                    </IonButtons>
                                </div>
                                </div>
                            </IonCol>
                        </IonRow> 
                    </IonGrid>
                </IonCard>

                <IonCard id="card">
                    <IonGrid>
                        <IonRow>
                            <IonCol id="col01">
                                <div id="col1">
                                    <h3 id="nomCol1">Spyder</h3>
                                    <h3 id="descCol1">3995 cc, Automatic petrol, 9.0 kmpl</h3>
                                </div>
                            </IonCol>
                        
                            <IonCol id="col02">
                            <div id="col2">
                                <h3 id="precCol2">$70,000.00</h3>
                                <div id="iconCol2">
                                    <IonButtons>
                                        Compare
                                        <IonIcon icon={stopOutline}/>
                                    </IonButtons>
                                </div>
                                </div>
                            </IonCol>
                        </IonRow> 
                    </IonGrid>
                </IonCard>

                <IonCard id="card">
                    <IonGrid>
                        <IonRow>
                            <IonCol id="col01">
                                <div id="col1">
                                    <h3 id="nomCol1">Cayman GT4</h3>
                                    <h3 id="descCol1">3995 cc, Automatic petrol, 9.0 kmpl </h3>
                                </div>
                            </IonCol>
                        
                            <IonCol id="col02">
                            <div id="col2">
                                <h3 id="precCol2">$72,000.00</h3>
                                <div id="iconCol2">
                                    <IonButtons>
                                        Compare
                                        <IonIcon icon={stopOutline}/>
                                    </IonButtons>
                                </div>
                                </div>
                            </IonCol>
                        </IonRow> 
                    </IonGrid>
                </IonCard>
            </div>

            <div id="div2">

                <h3 id="txt">Recomended for you</h3>

                <IonGrid>
                    <IonRow>
                        <IonCol id="colum">
                            <IonImg class="img2" src="assets/img/autos/Imagen2.png"></IonImg>
                            <div id="txt2">BMW 6 Series GT</div>
                        </IonCol>

                        <IonCol id="colum">
                            <IonImg class="img2" src="assets/img/autos/Imagen3.png"></IonImg>
                            <div id="txt2">Mercedes SLC</div>
                        </IonCol>

                        <IonCol id="colum">
                            <IonImg class="img2" src="assets/img/autos/Imagen4.png"></IonImg>
                            <div id="txt2">Continental</div>
                        </IonCol>
                    </IonRow>

                    <IonRow>
                        <IonCol id="colum">
                            <IonImg class="img2" src="assets/img/autos/Imagen5.png"></IonImg>
                            <div id="txt2">Acura NSX</div>
                        </IonCol>

                        <IonCol id="colum">
                            <IonImg class="img2" src="assets/img/autos/Imagen6.png"></IonImg>
                            <div id="txt2">Mercedes SLC</div>
                        </IonCol>

                        <IonCol id="colum">
                            <IonImg class="img2" src="assets/img/autos/Imagen7.png"></IonImg>
                            <div id="txt2">Continental</div>
                        </IonCol>
                    </IonRow>
                </IonGrid>
                
            </div>

            
            {/* <IonButton class="button button-block button-positive" id="btn">
                Get Offers from Dealers
            </IonButton> */}

            <div className="btnV3">
                <IonButton shape="round" size="large" fill="outline" id="btn" onClick={()=>history.push('/Vista4')}>
                    <div className="txtbtnV3">
                        Get Offers from Dealers
                    </div>
                </IonButton>   
            </div> 
           
            
        </IonContent>
        
    </IonPage>
}

export default Vista3;