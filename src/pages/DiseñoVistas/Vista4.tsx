import { IonButton, IonButtons, IonCard, IonCardContent, IonCardHeader, IonCardSubtitle, IonCardTitle, IonCol, IonContent, IonGrid, IonHeader, IonIcon, IonImg, IonPage, IonRow, IonTitle, IonToolbar } from "@ionic/react";
import { add, arrowBack, arrowForward, chevronDown, chevronForward, heartOutline, star, stopOutline } from "ionicons/icons";
import React from "react";
import { useHistory } from "react-router";
import './estilos/Vista4.css';

const Vista4: React.FC = () =>{

    const history = useHistory();

    return <IonPage>

        <IonHeader class="no-border" >
            <IonToolbar class="colorToolV4"  color="medium" >
                <IonButtons slot="start" onClick={() => history.push('/Vista3')}>
                    <IonIcon icon={arrowBack}/>
                </IonButtons>
                <IonTitle class="ion-text-center">Compare Cars</IonTitle>
            </IonToolbar>
        </IonHeader>

        <IonContent class="contentV4">

            <IonGrid class="gridV4">
                <IonRow>
                    <IonCol class="col1V4">
                        <IonCard class="cardV4">
                            <IonImg class="imgV4" src="assets/img/autos/Porsche 718.png"></IonImg>
                        </IonCard>
                    </IonCol>

                    <IonCol class="col2V4">
                        <div className="col2V4">
                            <h6 className="txt1col2V4">Porsche 718</h6>
                            <h6 className="txt2col2V4">Porsche/Luxury/The 2.3L EcoBoost</h6>
                            <h6 className="txt3col2V4">$62,000.00 - 74,000.00</h6>
                        </div>
                    </IonCol>
                </IonRow>
            </IonGrid>

            <div className="btnV4">
                <IonButton shape="round" size="large" fill="outline" color="success" onClick={() => history.push('/Vista5')}>
                    <div className="txtbtnV4">
                        <IonIcon class="iconV4" icon={add}></IonIcon>
                        Add Cars
                    </div>
                </IonButton>
            </div>


            <div>
                <IonCard class="card2V4">

                    <h3 className="txt4V4">Selected Similar Cars</h3>

                    <IonGrid>
                        <IonRow>
                            <IonCol>
                                <IonCard class="card3V4">
                                    <IonImg class="" src="assets/img/autos/Porsche2 718.png"></IonImg>
                                    <h3 className="nomV4">Mercedes SCL</h3>
                                    <h3 className="precV4">$42,70-$48,70</h3>
                                    <div  className="icon2V4">
                                        <IonButtons>
                                            <IonIcon icon={stopOutline}/>
                                            Compare
                                        </IonButtons>
                                    </div>
                                </IonCard>
                            </IonCol>

                            <IonCol>
                                <IonCard class="card3V4">
                                    <IonImg class="" src="assets/img/autos/polestar.png"></IonImg>
                                    <h3 className="nomV4">Polestar 1</h3>
                                    <h3 className="precV4">$42,70-$48,70</h3>
                                    <div  className="icon2V4">
                                        <IonButtons>
                                            <IonIcon icon={stopOutline}/>
                                            Compare
                                        </IonButtons>
                                    </div>
                                </IonCard>
                            </IonCol>
                        </IonRow>

                        <IonRow>
                            <IonCol>
                                <IonCard class="card3V4">
                                    <IonImg class="" src="assets/img/autos/continental.png"></IonImg>
                                    <h3 className="nomV4">Continental</h3>
                                    <h3 className="precV4">$42,70-$48,70</h3>
                                    <div  className="icon2V4">
                                        <IonButtons>
                                            <IonIcon icon={stopOutline}/>
                                            Compare
                                        </IonButtons>
                                    </div>
                                </IonCard>
                            </IonCol>

                            <IonCol>
                                <IonCard class="card3V4">
                                    <IonImg class="" src="assets/img/autos/LexusLC.png"></IonImg>
                                    <h3 className="nomV4">Lexus LC</h3>
                                    <h3 className="precV4">$42,70-$48,70</h3>
                                    <div  className="icon2V4">
                                        <IonButtons>
                                            <IonIcon icon={stopOutline}/>
                                            Compare
                                        </IonButtons>
                                    </div>
                                </IonCard>
                            </IonCol>
                        </IonRow>
                    </IonGrid>

                </IonCard>
            </div>

            
        </IonContent>
        
    </IonPage>
}

export default Vista4;