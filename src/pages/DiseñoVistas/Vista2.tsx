import { IonButton, IonButtons, IonCard, IonCardContent, IonCardHeader, IonCardSubtitle, IonCardTitle, IonCol, IonContent, IonGrid, IonHeader, IonIcon, IonImg, IonPage, IonRow, IonTitle, IonToolbar } from "@ionic/react";
import { arrowBack, arrowForward, chevronDown, chevronForward, heartOutline, star, stopOutline } from "ionicons/icons";
import React from "react";
import { useHistory } from "react-router";
import './estilos/estilos.css';

const Pantalla2: React.FC = () =>{

    const history = useHistory();

    return <IonPage>

        <IonHeader >
            <IonToolbar class="toolV2">
                <IonButtons class="" slot="start" onClick={() => history.push('/Vista1')}>
                    <IonIcon icon={arrowBack}/>
                </IonButtons>
                <IonTitle slot="start"></IonTitle>

                <IonButtons slot="end" class="">
                    Bangkok
                    <IonIcon icon={chevronDown}/>
                    <div className="icons">
                        <IonIcon icon={heartOutline}/>
                    </div>
                    <div className="icons">
                        <IonIcon onClick={() => history.push('/Vista3')} icon={arrowForward}/>
                    </div>
                </IonButtons>
            </IonToolbar>
        </IonHeader>

        <IonContent>
            <IonCard class="cardV2">
                <IonImg src="assets/img/autos/Porsche 718.png"></IonImg>

            </IonCard>

            <IonCard class="card2V2">
                <IonGrid class="gridV2">

                    <IonRow class="r1">
                        <IonCol class="">

                            <h5 className="nombreV2">Porsche 718</h5>
                            <IonCol>
                                <h5 className="precioV2">$62,000.00-74,000.00</h5>
                            </IonCol>
                            <IonCol class="col1V2">
                                <IonButtons class="icon2V2">
                                    <IonIcon icon={star}/>
                                    <IonIcon icon={star}/>
                                    <IonIcon icon={star}/>
                                    <IonIcon icon={star}/>
                                    <IonIcon icon={star}/>
                                    <h1 className="txticon2V2">268review</h1>
                                </IonButtons>
                            </IonCol>
                            <IonCol>
                                <h5 className="txtV2">Key Specs</h5>
                            </IonCol>

                        </IonCol>
                        <IonCol class="col2V2">

                            <IonButtons class="iconV2" onClick={() => history.push('/Vista2')}>
                                Compare
                                <IonIcon icon={stopOutline}/>
                            </IonButtons>
                            <IonButtons class="iconV2" onClick={() => history.push('/Vista2')}>
                                <IonIcon></IonIcon>
                            </IonButtons>
                            <IonButtons class="icon2V2" onClick={() => history.push('/Vista2')}>
                                <div className="icongreenV2">
                                    Rate This Car
                                    <IonIcon></IonIcon>
                                </div>
                            </IonButtons>
                            <IonButtons class="iconV2" onClick={() => history.push('/Vista2')}>
                                <IonIcon></IonIcon>
                            </IonButtons>
                            <IonButtons class="icon3V2" onClick={() => history.push('/Vista2')}>
                                All Specs
                                <IonIcon icon={chevronForward}> </IonIcon>
                            </IonButtons>
                            {/* <h5 className="iconV2">Porsche 718  <IonIcon icon={stopOutline}/></h5> */}
                        
                        </IonCol>
                    </IonRow>

                    <IonRow>
                        <IonCol>
                            <IonCard class="Card3V2">
                                <IonCardHeader>
                                    
                                    <IonImg class="img3V2" src="assets/img/autos/enginepower.png"></IonImg>
                                    
                                    <IonCardTitle class="Card3Title">3995 CC</IonCardTitle>
                                    <IonCardSubtitle  class="Card3Subtitle">Engine Power</IonCardSubtitle>
                                </IonCardHeader>
                            </IonCard>
                        </IonCol>

                        <IonCol>
                        <IonCard class="Card3V2">
                            <IonCardHeader>
                                
                                <IonImg class="img3V2" src="assets/img/autos/torque2.png"></IonImg>
                                
                                <IonCardTitle class="Card3Title">360 N·m</IonCardTitle>
                                <IonCardSubtitle  class="Card3Subtitle">Max Torque</IonCardSubtitle>
                            </IonCardHeader>
                        </IonCard>
                        </IonCol>

                        <IonCol>
                            <IonCard class="Card3V2">
                            <IonCardHeader>
                                
                                <IonImg class="img3V2" src="assets/img/autos/aceleration.png"></IonImg>
                                
                                <IonCardTitle class="Card3Title">0-100km/h-4s</IonCardTitle>
                                <IonCardSubtitle  class="Card3Subtitle">Acceleration</IonCardSubtitle>
                            </IonCardHeader>
                        </IonCard>
                        </IonCol>
                    </IonRow>
                </IonGrid>
                
            </IonCard>

            {/* Lo mismo que tiene la vista 3 */}

            <div className="vista3">

                <IonButtons class="menu">
                    <h6 onClick={()=>history.push('/Vista1')} className="menu">Price</h6>
                    <h6 className="menuu">Reviews</h6>
                    <h6 className="menuu">FAQ</h6>
                </IonButtons>

                <IonCard id="card">
                    <IonGrid>
                        <IonRow>
                            <IonCol id="col01">
                                <div id="col1">
                                    <h3 id="nomCol1">Cayman</h3>
                                    <h3 id="descCol1">1988 cc, Automatic petrol, 9.0 kmpl </h3>
                                </div>
                            </IonCol>
                        
                            <IonCol id="col02">
                            <div id="col2">
                                <h3 id="precCol2">$62,000.00</h3>
                                <div id="iconCol2">
                                    <IonButtons>
                                        Compare
                                        <IonIcon icon={stopOutline}/>
                                    </IonButtons>
                                </div>
                                </div>
                            </IonCol>
                        </IonRow> 
                    </IonGrid>
                </IonCard>

                <IonCard id="card">
                    <IonGrid>
                        <IonRow>
                            <IonCol id="col01">
                                <div id="col1">
                                    <h3 id="nomCol1">Boxter</h3>
                                    <h3 id="descCol1">1988 cc, Automatic petrol, 9.0 kmpl </h3>
                                </div>
                            </IonCol>
                        
                            <IonCol id="col02">
                            <div id="col2">
                                <h3 id="precCol2">$68,000.00</h3>
                                <div id="iconCol2">
                                    <IonButtons>
                                        Compare
                                        <IonIcon icon={stopOutline}/>
                                    </IonButtons>
                                </div>
                                </div>
                            </IonCol>
                        </IonRow> 
                    </IonGrid>
                </IonCard>

                <IonCard id="card">
                    <IonGrid>
                        <IonRow>
                            <IonCol id="col01">
                                <div id="col1">
                                    <h3 id="nomCol1">Spyder</h3>
                                    <h3 id="descCol1">3995 cc, Automatic petrol, 9.0 kmpl</h3>
                                </div>
                            </IonCol>
                        
                            <IonCol id="col02">
                            <div id="col2">
                                <h3 id="precCol2">$70,000.00</h3>
                                <div id="iconCol2">
                                    <IonButtons>
                                        Compare
                                        <IonIcon icon={stopOutline}/>
                                    </IonButtons>
                                </div>
                                </div>
                            </IonCol>
                        </IonRow> 
                    </IonGrid>
                </IonCard>

                <IonCard id="card">
                    <IonGrid>
                        <IonRow>
                            <IonCol id="col01">
                                <div id="col1">
                                    <h3 id="nomCol1">Cayman GT4</h3>
                                    <h3 id="descCol1">3995 cc, Automatic petrol, 9.0 kmpl </h3>
                                </div>
                            </IonCol>
                        
                            <IonCol id="col02">
                            <div id="col2">
                                <h3 id="precCol2">$72,000.00</h3>
                                <div id="iconCol2">
                                    <IonButtons>
                                        Compare
                                        <IonIcon icon={stopOutline}/>
                                    </IonButtons>
                                </div>
                                </div>
                            </IonCol>
                        </IonRow> 
                    </IonGrid>
                </IonCard>
             </div>

            <div id="div2">

                <h3 id="txt">Recomended for you</h3>

                <IonGrid>
                    <IonRow>
                        <IonCol id="colum">
                            <IonImg class="img2" src="assets/img/autos/Imagen2.png"></IonImg>
                            <div id="txt2">BMW 6 Series GT</div>
                        </IonCol>

                        <IonCol id="colum">
                            <IonImg class="img2" src="assets/img/autos/Imagen3.png"></IonImg>
                            <div id="txt2">Mercedes SLC</div>
                        </IonCol>

                        <IonCol id="colum">
                            <IonImg class="img2" src="assets/img/autos/Imagen4.png"></IonImg>
                            <div id="txt2">Continental</div>
                        </IonCol>
                    </IonRow>

                    <IonRow>
                        <IonCol id="colum">
                            <IonImg class="img2" src="assets/img/autos/Imagen5.png"></IonImg>
                            <div id="txt2">Acura NSX</div>
                        </IonCol>

                        <IonCol id="colum">
                            <IonImg class="img2" src="assets/img/autos/Imagen6.png"></IonImg>
                            <div id="txt2">Mercedes SLC</div>
                        </IonCol>

                        <IonCol id="colum">
                            <IonImg class="img2" src="assets/img/autos/Imagen7.png"></IonImg>
                            <div id="txt2">Continental</div>
                        </IonCol>
                    </IonRow>
                </IonGrid>

            </div>


                {/* <IonButton class="button button-block button-positive" id="btn">
                Get Offers from Dealers
                </IonButton> */}

            <div className="btnV3">
                <IonButton shape="round" size="large" fill="outline" id="btn" onClick={()=>history.push('/Vista4')}>
                    <div className="txtbtnV3">
                        Get Offers from Dealers
                    </div>
                </IonButton>   
            </div> 

        </IonContent>
        
    </IonPage>
}

export default Pantalla2;