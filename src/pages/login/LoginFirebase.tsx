import { IonButton, IonButtons, IonCheckbox, IonCol, IonContent, IonGrid, IonHeader, IonInput, IonItem, 
    IonItemDivider, IonLabel, IonList, IonMenu, IonPage, IonRow, IonTitle, IonToolbar } from "@ionic/react";
import { createUserWithEmailAndPassword, getAuth, signInWithEmailAndPassword } from "firebase/auth";
import React, { useRef } from "react";
import { useHistory } from "react-router-dom";
import swal from 'sweetalert';

const LoginFirebase: React.FC = () => {

    const history = useHistory();

    const emailRef = useRef<HTMLIonInputElement>(null);
    const passRef = useRef<HTMLIonInputElement>(null);

    const login = async() => {

        const email = emailRef.current!.value as string;
        const password = passRef.current!.value as string;

        const auth = getAuth();
        signInWithEmailAndPassword(auth, email, password)
        .then(() => {
            // Signed in
            // const user = userCredential.user;
            // alert(user);
            history.push('/mostrar');
            // ...
        })
        .catch((error) => {
            /*const errorCode = error.code;
            const errorMessage = error.message;*/
            swal({
                title: "Verifique las credenciales",
                icon: "warning",
            });
        });
    }

    return <IonPage>

<IonHeader>
            <IonToolbar>
                <IonTitle>Login</IonTitle>
                    <IonButtons slot="end">
                        <IonButton href="/SignUp">Registrarse</IonButton>
                    </IonButtons>
            </IonToolbar>
        </IonHeader>

        <IonContent class="">
            <form className="ion-padding">
                <IonItem>
                    <IonLabel  position="floating">Email: </IonLabel>
                        <IonInput type="text" id="user" ref={emailRef}/>
                </IonItem>
                <IonItem>
                    <IonLabel  position="floating">Contraseña</IonLabel>
                    <IonInput type="password" id="pass" ref={passRef}/>
                </IonItem>

                <IonButton onClick={login} expand="block">
                    Login
                </IonButton>
                
            </form>
        </IonContent>

    </IonPage>

}

export default LoginFirebase;