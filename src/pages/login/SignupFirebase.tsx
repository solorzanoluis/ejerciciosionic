import { IonButton, IonButtons, IonCheckbox, IonCol, IonContent, IonFabButton, IonGrid, IonHeader, IonInput, IonItem, 
    IonItemDivider, IonLabel, IonList, IonMenu, IonPage, IonRow, IonTitle, IonToolbar } from "@ionic/react";
import React, { useRef } from "react";
import { getAuth, createUserWithEmailAndPassword } from "firebase/auth";
import { initializeApp } from "firebase/app";
import { useHistory } from "react-router-dom";
import swal from 'sweetalert';

const SignUp: React.FC = () =>{

    const history = useHistory();

    const firebaseConfig = {
        apiKey: "AIzaSyD_D84yaRWhkyalQgErNifrSPhWrbT3U9s",
        authDomain: "crudionic-a43d5.firebaseapp.com",
        databaseURL: "https://crudionic-a43d5-default-rtdb.firebaseio.com",
        projectId: "crudionic-a43d5",
        storageBucket: "crudionic-a43d5.appspot.com",
        messagingSenderId: "289561271738",
        appId: "1:289561271738:web:0ba00eb1b69547dcef9183",
        measurementId: "G-S5VF0K8VM4"
    };
      
    // Initialize Firebase
    const app = initializeApp(firebaseConfig);

    const emailRef = useRef<HTMLIonInputElement>(null);
    const passRef = useRef<HTMLIonInputElement>(null);

    
    

    const agregar = async() => {

        const email = emailRef.current!.value as string;
        const password = passRef.current!.value as string;

        if(email === '' && password ===''){
            return swal({
                //title: "Debe r",
                text: "Debe rellenar los campos",
                icon: "warning",
                //buttons: true,
                dangerMode: true,
            }).then(() => {
                history.push('/SignUp');
            })
        }

        const auth = getAuth();
        createUserWithEmailAndPassword(auth, email,password)
        .then((userCredential) => {
        
            if(userCredential){

                return swal({
                    
                    text: "Registro exitoso",
                    icon: "success"
                }).then(() => {
                    history.push('/SignIn');
                });

            }

        })
        .catch((error) => {
        // const errorCode = error.code;
        // const errorMessage = error.message;
            swal({
                title: "Verifique las credenciales ingresadas.",
                text: "-Debe ser un correo válido \n-La contraseña debe ser mayor a 6 caracteres.",
                icon: "warning",
                //buttons: true,
                dangerMode: true,
            });
        });

        //alert('Creado');

    }

    return <IonPage>

        <IonHeader>
            <IonToolbar color="secondary">
                <IonTitle>Registrarse</IonTitle>
                <IonButtons slot="end">
                        <IonButton href="/SignIn">Login</IonButton>
                </IonButtons>
            </IonToolbar>
        </IonHeader>

        <IonContent>

            <form className="ion-padding">
                <IonItem>
                    <IonLabel  position="floating">Email</IonLabel>
                    <IonInput type="email" id="user" ref={emailRef}/>
                </IonItem>
                <IonItem>
                    <IonLabel position="floating">Contraseña</IonLabel>
                    <IonInput type="text" id="user" minlength={6} maxlength={9} ref={passRef}/>
                </IonItem>
                {/* <IonButton className="ion-margin-top" type="submit" expand="block" onClick={agregar}>
                    Registrarse
                </IonButton> */}
                <IonFabButton slot="start" className="ion-margin-top" type="submit" onClick={agregar}>
                    Add
                </IonFabButton>
                    
            </form>

        </IonContent>

    </IonPage>
}

export default SignUp;