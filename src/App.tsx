import { Redirect, Route } from 'react-router-dom';
import { IonApp, IonRouterOutlet, setupIonicReact } from '@ionic/react';
import { IonReactRouter } from '@ionic/react-router';
import Home from './pages/Home';

/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';

/* Theme variables */
import './theme/variables.css';
import AgregarFirebase from './pages/crud/AgregarFirebase';
import ListarFirebase from './pages/crud/ListadoFirebase';
import ActualizarFirebase from './pages/crud/ActualizarFirebase';
import SignUp from './pages/login/SignupFirebase';
import LoginFirebase from './pages/login/LoginFirebase';
import  Vista1  from './pages/DiseñoVistas/Vista1';
import { Pruebas } from './pages/DiseñoVistas/Prueba';
import Pantalla2 from './pages/DiseñoVistas/Vista2';
import Vista3 from './pages/DiseñoVistas/Vista3';
import Vista4 from './pages/DiseñoVistas/Vista4';
import Vista5 from './pages/DiseñoVistas/Vista5';
import Vista6 from './pages/DiseñoVistas/Vista6';

setupIonicReact();

const App: React.FC = () => (
  <IonApp>
    <IonReactRouter>
      <IonRouterOutlet>

        {/* <Route exact path="/home">
          <Home />
        </Route>
        <Route exact path="/">
          <Redirect to="/home" />
        </Route> */}

        {/* ----------------------Rutas crud con Firebase---------------------- */}
       
        <Route exact path="/firebase" component={AgregarFirebase}></Route>
        <Route exact path="/mostrar" component={ListarFirebase}></Route>
        <Route exact path="/actualizar/:id/:nom" component={ActualizarFirebase}/>
        <Route exact path="/SignUp" component={SignUp}></Route>
        <Route exact path="/SignIn" component={LoginFirebase}></Route>


        {/* ----------------------Rutas diseños----------------------------------- */}
        <Route exact path="/prueba" component={Pruebas}></Route>
        <Route exact path="/Vista1" component={Vista1}></Route>
        <Route exact path="/Vista2" component={Pantalla2}></Route>
        <Route exact path="/Vista3" component={Vista3}></Route>
        <Route exact path="/Vista4" component={Vista4}></Route>
        <Route exact path="/Vista5" component={Vista5}></Route>
        <Route exact path="/Vista6" component={Vista6}></Route>

        <Redirect to="/Vista6" />

      </IonRouterOutlet>
    </IonReactRouter>
  </IonApp>
);

export default App;
